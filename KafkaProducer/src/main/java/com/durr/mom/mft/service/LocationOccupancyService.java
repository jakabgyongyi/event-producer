package com.durr.mom.mft.service;

import java.util.Objects;

import org.springframework.core.env.Environment;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import com.durr.mom.mft.generated.locationOccupancy.models.LocationOccupancyEventDto;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class LocationOccupancyService {

	private KafkaTemplate<String, String> kafkaTemplate;
	private Environment environment;
	
	public void sendEvent(LocationOccupancyEventDto eventDto) {
		kafkaTemplate.send(Objects.requireNonNull(environment.getProperty("kafka-broker.topic")), new Gson().toJson(eventDto));
	}
}
