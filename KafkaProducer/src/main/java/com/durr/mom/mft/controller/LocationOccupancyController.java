package com.durr.mom.mft.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.durr.mom.mft.generated.locationOccupancy.api.LocationOccupancyApi;
import com.durr.mom.mft.generated.locationOccupancy.models.LocationOccupancyEventDto;
import com.durr.mom.mft.service.LocationOccupancyService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class LocationOccupancyController implements LocationOccupancyApi {
	
	private LocationOccupancyService locationOccupancyService;
	
	@Override
	public ResponseEntity<Void> postLocationOccupancyEvent(@Valid LocationOccupancyEventDto locationOccupancyEventDto) {
		locationOccupancyService.sendEvent(locationOccupancyEventDto);
		return ResponseEntity.noContent().build();
	}
}
